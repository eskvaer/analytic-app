# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import logging

from django.conf import settings
from django.db.models import Count
from django.http import Http404
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_GET
from django.utils.decorators import method_decorator
from django.utils.timezone import get_current_timezone, now as timezone_now
from django.utils.translation import ugettext_lazy as _

from apiclient.discovery import build
from dateutil.relativedelta import relativedelta
from googleapiclient.errors import HttpError
from oauth2client.client import AccessTokenRefreshError
from oauth2client.service_account import ServiceAccountCredentials

from core.views import TemplateView
from locations.models import Location
from misc.helpers import (
    get_tz_datetime_min, get_tz_datetime_max, generate_date_range, json_resp,
)
from misc.utils import psql_construct_select_date
from misc.decorators import ajax_required
from users.constants import USER_STATUS
from users.decorators import permit_user_type
from users.models import Consumer, ConsumerSocialUID, User

from .utils import get_google_installs, get_itunes_downloads


LOG = logging.getLogger(__name__)


DATE_FORMAT = '%Y-%m-%d'


def get_first_profile_id(service):
    accounts = service.management().accounts().list().execute()

    if accounts.get('items'):
        first_account_id = accounts.get('items')[0].get('id')

        web_properties = (
            service
            .management()
            .webproperties()
            .list(accountId=first_account_id)
            .execute()
        )

        if web_properties.get('items'):
            first_web_property_id = web_properties.get('items')[0].get('id')

            profiles = (
                service
                .management()
                .profiles()
                .list(
                    accountId=first_account_id,
                    webPropertyId=first_web_property_id,
                )
                .execute()
            )

        if profiles.get('items'):
            return profiles.get('items')[0].get('id')

    return None


class ActivityListView(TemplateView):
    template_name = 'analytics/pages/activity_list.html'
    title = _("Active users")

    @method_decorator(login_required)
    @method_decorator(permit_user_type(User.types.PRINCIPAL))
    def dispatch(self, request, *args, **kwargs):
        self.context['stats_to_date'] = (
            timezone_now().date() - relativedelta(days=1)
        )

        return super(ActivityListView, self).dispatch(request, *args, **kwargs)


class AjaxGetUniqueUsersActivityView(TemplateView):

    @method_decorator(require_GET)
    @method_decorator(login_required)
    @method_decorator(permit_user_type(User.types.PRINCIPAL))
    def dispatch(self, request, *args, **kwargs):
        return super(AjaxGetUniqueUsersActivityView, self).dispatch(
            request, *args, **kwargs
        )

    def get(self, request, *args, **kwargs):
        if request.is_ajax:
            period = request.GET.get('period', 1)

            end_date = timezone_now().date()
            start_date = end_date - relativedelta(months=int(period))

            credentials = ServiceAccountCredentials.from_json_keyfile_name(
                settings.GA_SECRETS_PATH,
                'https://www.googleapis.com/auth/analytics.readonly'
            )
            service = build('analytics', 'v3', credentials=credentials)

            try:
                first_profile_id = get_first_profile_id(service)

                if not first_profile_id:
                    LOG.error('Could not find a valid profile for this user.')
                else:
                    activity = []

                    ga_data = (
                        service
                        .data()
                        .ga()
                        .get(
                            ids='ga:{0}'.format(first_profile_id),
                            start_date=start_date.strftime(DATE_FORMAT),
                            end_date=end_date.strftime(DATE_FORMAT),
                            metrics='ga:users',
                            dimensions='ga:date, ga:userType',
                            sort='ga:date',
                        )
                        .execute()
                    )
                    if ga_data:
                        rows = ga_data.get('rows')

                        new_visitors = {}
                        returning_visitors = {}

                        for row in rows:
                            if row[1] == 'New Visitor':
                                new_visitors[row[0]] = row[2]
                            if row[1] == 'Returning Visitor':
                                returning_visitors[row[0]] = row[2]

                        calc_dates_range = (
                            generate_date_range(
                                start_date, end_date, include_last_day=True
                            )
                        )
                        for date in calc_dates_range:
                            date_key = date.strftime('%Y%m%d')
                            activity.append([
                                date.strftime(DATE_FORMAT),
                                new_visitors.get(date_key, 0),
                                returning_visitors.get(date_key, 0),
                            ])

                    return json_resp({'activity': activity, 'status': 'success'})
            except TypeError as error:
                # Handle errors in constructing a query.
                LOG.error(
                    'There was an error in constructing your query : {0}'
                    .format(error)
                )

            except HttpError as error:
                # Handle API errors.
                LOG.error(
                    'Arg, there was an API error : {0} : {1}'
                    .format(error.resp.status, error._get_reason())
                )

            except AccessTokenRefreshError:
                # Handle Auth errors.
                LOG.error(
                    'The credentials have been revoked or expired, '
                    'please re-run the application to re-authorize'
                )
        else:
            raise Http404


# -----------------------------------------------------------------------------

class RegistrationsListView(TemplateView):
    template_name = 'analytics/pages/registrations_list_as_principal.html'
    title = _('User registration statistics')

    @method_decorator(login_required)
    @method_decorator(permit_user_type(
        User.types.PRINCIPAL, User.types.AGENCY_ADMIN
    ))
    def dispatch(self, request, *args, **kwargs):
        return super(RegistrationsListView, self).dispatch(
            request, *args, **kwargs
        )

    def _process_principal(self, request):
        consumer_social_qs = (
            ConsumerSocialUID.objects
            .filter(consumer__user_status=USER_STATUS.ACTIVE)
            .exclude(
                consumer__is_registered_by_email=True,
                consumer__is_email_confirmed=False,
            )
        )
        count_vk_consumers = (
            consumer_social_qs.filter(provider__code='VK').count()
        )
        count_gp_consumers = (
            consumer_social_qs.filter(provider__code='GP').count()
        )
        count_fb_consumers = (
            consumer_social_qs.filter(provider__code='FB').count()
        )

        alive_consumer_qs = Consumer.objects.alive()
        total_consumers = alive_consumer_qs.count()
        count_email_reg_consumers = (
            alive_consumer_qs.filter(is_registered_by_email=True).count()
        )

        google_installs = get_google_installs()
        itunes_downloads = get_itunes_downloads()
        total_downloads = None

        if not (itunes_downloads is None or google_installs is None):
            total_downloads = itunes_downloads + google_installs

        diff_consumers = total_consumers - total_downloads
        try:
            diff_percents = int(diff_consumers * 100 / total_consumers)
        except ZeroDivisionError:
            diff_percents = 0

        self.context['diff_consumers'] = diff_consumers
        self.context['diff_percents'] = diff_percents
        self.context['total_consumers'] = total_consumers
        self.context['count_vk_consumers'] = count_vk_consumers
        self.context['count_gp_consumers'] = count_gp_consumers
        self.context['count_fb_consumers'] = count_fb_consumers
        self.context['count_email_reg_consumers'] = count_email_reg_consumers
        self.context['total_downloads'] = total_downloads
        self.context['itunes_downloads'] = itunes_downloads
        self.context['google_installs'] = google_installs
        self.context['cities'] = Location.objects.cities().order_by('name')

    def _process_agency_admin(self, request):
        self.template_name = 'analytics/pages/registrations_list_as_agency_admin.html'

    def get(self, request, *args, **kwargs):
        suffix = self.user_type.name.lower()

        processor_name = "_process_{0}".format(suffix)
        if hasattr(self, processor_name):
            processor = getattr(self, processor_name)
            processor(request)
        else:
            raise Http404()

        return super(RegistrationsListView, self).get(request, *args, **kwargs)


class AjaxGetTotalUserRegistrationsView(TemplateView):

    @method_decorator(require_GET)
    @method_decorator(ajax_required)
    @method_decorator(login_required)
    @method_decorator(permit_user_type(
        User.types.PRINCIPAL, User.types.AGENCY_ADMIN
    ))
    def dispatch(self, request, *args, **kwargs):
        return super(AjaxGetTotalUserRegistrationsView, self).dispatch(
            request, *args, **kwargs
        )

    def _process_principal(self, request):
        self.city = request.GET.get('city')

    def _process_agency_admin(self, request):
        self.city = self.user.profile.agency.city_id

    def get(self, request, *args, **kwargs):
        suffix = self.user_type.name.lower()

        processor_name = "_process_{0}".format(suffix)
        if hasattr(self, processor_name):
            processor = getattr(self, processor_name)
            processor(request)
        else:
            raise Http404()

        # ---------------------------------------------------------------------

        city = self.city
        period = request.GET.get('period', 1)

        end_date = timezone_now().date()
        start_date = end_date - relativedelta(months=int(period))

        current_tz = get_current_timezone()
        calc_date_begin = get_tz_datetime_min(start_date, current_tz)

        social_qs = (
            ConsumerSocialUID.objects
            .filter(consumer__user_status=USER_STATUS.ACTIVE)
            .exclude(
                consumer__is_registered_by_email=True,
                consumer__is_email_confirmed=False,
            )
        )
        alive_consumers_qs = Consumer.objects.alive()
        if city:
            social_qs = social_qs.filter(consumer__city=city)
            alive_consumers_qs = alive_consumers_qs.filter(city=city)

        dates_counter = (
            alive_consumers_qs.filter(date_joined__gte=calc_date_begin)
            .extra({'joined': psql_construct_select_date('date_joined')})
            .values('joined')
            .annotate(c=Count('id'))
        )
        dates_counter = {
            r['joined'].strftime(DATE_FORMAT): r['c'] for r in dates_counter
        }

        count_registered_consumers = (
            alive_consumers_qs.filter(date_joined__lte=calc_date_begin).count()
        )
        calc_dates_range = (
            generate_date_range(start_date, end_date, include_last_day=True)
        )
        increase_registrations = []
        for date in calc_dates_range:
            date_key = date.strftime(DATE_FORMAT)
            count_registered_consumers += dates_counter.get(date_key, 0)
            increase_registrations.append({
                'date': date_key,
                'amount': count_registered_consumers
            })

        social_regs = list(
            social_qs
            .extra({'joined': psql_construct_select_date(
                                            'users_consumer.date_joined')})
            .values_list('provider__code')
            .annotate(c=Count('id'))
        )
        registrations = []
        count_social_regs = 0
        for r in social_regs:
            soc_regs = r[1]
            count_social_regs += soc_regs
            registrations.append({r[0]: soc_regs})
        count_email_reg_consumers = (
            alive_consumers_qs.filter(is_registered_by_email=True).count()
        )
        registrations.append({'Email': count_email_reg_consumers})

        data = {
            'status': 'success',
            'increase_registrations': increase_registrations,
            'registrations': registrations,
        }

        return json_resp(data)


class AjaxGetDailyUserRegistrationsView(TemplateView):

    @method_decorator(require_GET)
    @method_decorator(ajax_required)
    @method_decorator(login_required)
    @method_decorator(permit_user_type(
        User.types.PRINCIPAL, User.types.AGENCY_ADMIN
    ))
    def dispatch(self, request, *args, **kwargs):
        return super(AjaxGetDailyUserRegistrationsView, self).dispatch(
            request, *args, **kwargs
        )

    def _process_principal(self, request):
        self.city = request.GET.get('city')

    def _process_agency_admin(self, request):
        self.city = self.user.profile.agency.city_id

    def get(self, request, *args, **kwargs):
        suffix = self.user_type.name.lower()

        processor_name = "_process_{0}".format(suffix)
        if hasattr(self, processor_name):
            processor = getattr(self, processor_name)
            processor(request)
        else:
            raise Http404()

        # ---------------------------------------------------------------------

        city = self.city
        period = request.GET.get('period', 1)

        end_date = timezone_now().date()
        start_date = end_date - relativedelta(months=int(period))

        current_tz = get_current_timezone()
        calc_date_begin = get_tz_datetime_min(start_date, current_tz)
        calc_date_end = get_tz_datetime_max(end_date, current_tz)

        social_qs = ConsumerSocialUID.objects.filter(
            consumer__user_status=USER_STATUS.ACTIVE,
            consumer__date_joined__gte=calc_date_begin,
            consumer__date_joined__lte=calc_date_end,
        )
        total_qs = (
            Consumer.objects.alive().filter(
                date_joined__gte=calc_date_begin,
                date_joined__lte=calc_date_end,
            )
        )

        if city:
            social_qs = social_qs.filter(consumer__city=city)
            total_qs = total_qs.filter(city=city)

        social_regs = (
            social_qs
            .extra({'joined': psql_construct_select_date(
                                            'users_consumer.date_joined')})
            .values('provider__code', 'joined')
            .annotate(c=Count('id'))
        )
        social_regs = {
            (r['joined'], r['provider__code']): r['c'] for r in social_regs
        }

        total_regs = (
            total_qs
            .extra({'joined': psql_construct_select_date('date_joined')})
            .values('joined')
            .annotate(c=Count('id'))
        )
        total_regs = {r['joined']: r['c'] for r in total_regs}

        daily_regs = []

        calc_dates_range = (
            generate_date_range(start_date, end_date, include_last_day=True)
        )
        for date in calc_dates_range:
            count_total_regs = total_regs.get(date, 0)
            count_vk_regs = social_regs.get((date, 'VK'), 0)
            count_fb_regs = social_regs.get((date, 'FB'), 0)
            count_gp_regs = social_regs.get((date, 'GP'), 0)
            count_social_consumers = (
                count_vk_regs + count_fb_regs + count_gp_regs
            )
            daily_regs.append({
                'date': date.strftime(DATE_FORMAT),
                'total': count_total_regs,
                'VK': count_vk_regs,
                'FB': count_fb_regs,
                'GP': count_gp_regs,
                'Email': count_total_regs - count_social_consumers,
            })

        return json_resp({'status': 'success', 'daily_regs': daily_regs})
