# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import codecs
import csv
import logging
import os
from httplib2 import HttpLib2Error

from django.conf import settings
from django.utils.timezone import now as timezone_now

import redis
import requests
from apiclient import errors
from apiclient.discovery import build
from apiclient.http import MediaIoBaseDownload
from dateutil.relativedelta import relativedelta
from oauth2client.service_account import ServiceAccountCredentials

from misc.redis_pool import REDIS_STATS_POOL


LOG = logging.getLogger(__name__)


# -----------------------------------------------------------------------------
# Google Play
# -----------------------------------------------------------------------------

def _save_google_report_csv(filename, period=0):
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        settings.GA_REPORT_SECRETS_PATH,
        'https://www.googleapis.com/auth/devstorage.read_only'
    )
    storage = build('storage', 'v1', credentials=credentials)

    current_month = timezone_now().date() - relativedelta(months=period)
    report_to_download = (
        'stats/installs/installs_ua.com.discontimo.discontimo_{0}_overview.csv'
        .format(current_month.strftime('%Y%m'))
    )
    res = (
        storage.objects()
        .get_media(
            bucket=settings.CLOUD_STORAGE_BUCKET, object=report_to_download
        )
    )
    f = file(filename, 'w')
    downloader = MediaIoBaseDownload(f, res, chunksize=1024*1024)
    done = False
    while not done:
        try:
            status, done = downloader.next_chunk()
        except errors.HttpError as error:
            LOG.error('An error occurred: {0}'.format(error))
            return _save_google_report_csv(filename, 1)
        except HttpLib2Error as error:
            LOG.error('An error occurred: {0}'.format(error))
            break
    return done


def cache_mobile_app_installs(key_name, ttl=60*60*24):  # 1 day
    def wrapper(func):
        def new_func(*args, **kwargs):
            r = redis.Redis(connection_pool=REDIS_STATS_POOL)

            key = '{0}_{1}'.format(key_name, timezone_now().date().isoformat())

            installs = r.get(key)

            if kwargs.pop('force', False) or installs in ('None', None):
                installs = func()
                r.set(key, installs, ttl)

            return int(installs)
        return new_func
    return wrapper


@cache_mobile_app_installs('google_installs')
def get_google_installs():
    filename = 'google_report.csv'

    if not _save_google_report_csv(filename) or not os.path.exists(filename):
        return None

    csv_reader = csv.DictReader(codecs.open(filename, 'rU', 'utf-16'))
    last_date = None
    installs = 0

    for row in csv_reader:
        if not last_date or (last_date < row['Date']):
            last_date = row['Date']
            installs = row['Total User Installs']

    os.remove(filename)

    return int(installs)


# -----------------------------------------------------------------------------
# iTunes
# -----------------------------------------------------------------------------

def itunes_request(uri, **querystring_params):
    headers = {'X-Client-Key': settings.APP_FIGURES_APP_KEY}
    auth = (settings.APP_FIGURES_USERNAME, settings.APP_FIGURES_PASSWORD)
    return requests.get(
        'https://api.appfigures.com/v2/{0}'.format(uri.lstrip('/')),
        auth=auth,
        params=querystring_params,
        headers=headers,
    )


@cache_mobile_app_installs('itunes_downloads', ttl=60*60*12)  # 12 hours
def get_itunes_downloads():
    # Get the root resource to show we are in business
    root_response = itunes_request('/')
    data = 0
    if root_response.ok:
        # Get a list of products
        product_response = itunes_request('/products/mine')
        if product_response.ok:
            products = product_response.json()
            if 0 < len(products):
                # Get data for all inapps for a year by month
                products = products.values()
                inapps = [p for p in products if p['type'] == u'app']
                inapp_ids = [str(product['id']) for product in inapps]
                route = '/sales/?countries=UA'
                inapp_sales_response = itunes_request(
                    route,
                    granularity='monthly',
                    products=';'.join(inapp_ids),
                )
                if inapp_sales_response.ok:
                    data = (inapp_sales_response.json())['downloads']
    return int(data)
