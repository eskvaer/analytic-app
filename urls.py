# -*- coding: utf-8 -*-

from django.conf.urls import patterns, url

from views import (
    ActivityListView, AjaxGetUniqueUsersActivityView,
    RegistrationsListView, AjaxGetTotalUserRegistrationsView,
    AjaxGetDailyUserRegistrationsView,
)


urlpatterns = patterns(
    '',

    url(r'^activity/$',
        view=ActivityListView.as_view(),
        name='activity-list'),

    url(r'^activity/ajax/get-activity-unique-users/$',
        view=AjaxGetUniqueUsersActivityView.as_view(),
        name='ajax-get-unique-users-activity'),

    # -------------------------------------------------------------------------

    url(r'^registrations/$',
        view=RegistrationsListView.as_view(),
        name='registrations-users-list'),

    url(r'^activity/ajax/get-total-user-registrations/$',
        view=AjaxGetTotalUserRegistrationsView.as_view(),
        name='ajax-get-total-user-registrations'),

    url(r'^activity/ajax/get-daily-user-registrations/$',
        view=AjaxGetDailyUserRegistrationsView.as_view(),
        name='ajax-get-daily-user-registrations'),
)
